function worker(gen, ...args) {
    const generator = gen(...args);
  
    function second(val) {
      retValue = generator.next(val);
  
      if (typeof retValue.value === 'function') {
        let fnResult = retValue.value();
        return second(fnResult);
      }
      if (retValue.value instanceof Promise) {
        return Promise.resolve(retValue.value).then(val => second(val));
      }
      if (retValue.done) {
        return retValue.value;
      }
      return second(retValue.value);
    }
  
  return second();
}